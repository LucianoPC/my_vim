GIT_DIR=`pwd`

echo "Copy .irbrc from $GIT_DIR to ~/"
cd ~/
rm -rf .irbrc
ln -s $GIT_DIR/.irbrc

echo "Copy .vimrc from $GIT_DIR to ~/"
cd ~/
rm -rf .vimrc
ln -s $GIT_DIR/.vimrc

echo "Create ~/.vim/autoload and ~/.vim/bundle folder and prepare autoload"
cd ~/
rm -rf .vim/
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

echo "Clone NerdTree repository"
cd ~/.vim/bundle
git clone https://github.com/scrooloose/nerdtree.git

echo "Clone flake8"
cd ~/.vim/bundle
git clone https://github.com/nvie/vim-flake8.git

echo "Clone Vim-Ruby"
cd ~/.vim/bundle
git clone https://github.com/vim-ruby/vim-ruby.git

echo "Clone Fuzzy Finder"
cd ~/.vim/bundle
git clone https://github.com/junegunn/fzf
