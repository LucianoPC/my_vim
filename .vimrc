"execute pathogen#infect()

autocmd BufWritePre * :%s/\s\+$//e
retab

nmap <C-t> :tabnew<CR>
nmap <C-n> :Ntree<CR>
nmap <C-p> :FZF<CR>
nmap <C-h> <C-w>h
nmap <C-l> <C-w>l

nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

"Syntax Color
syntax on

"Line Number
set number

"Always in paste mode
"set paste

"Indentation whith Spaces
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent

"Indentation per language
autocmd FileType html       setlocal shiftwidth=2 tabstop=2 smartindent
autocmd FileType css        setlocal shiftwidth=2 tabstop=2 smartindent
autocmd FileType ruby       setlocal shiftwidth=2 tabstop=2 smartindent
autocmd FileType yaml       setlocal shiftwidth=2 tabstop=2 smartindent
autocmd FileType perl       setlocal shiftwidth=2 tabstop=2 smartindent
autocmd FileType python     setlocal shiftwidth=4 tabstop=4 smartindent
autocmd FileType lua        setlocal shiftwidth=4 tabstop=4 smartindent
autocmd FileType c          setlocal shiftwidth=4 tabstop=4 smartindent
autocmd FileType cpp        setlocal shiftwidth=4 tabstop=4 smartindent
autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 smartindent

"Vim-Ruby
set nocompatible      " We're running Vim, not Vi!
filetype on           " Enable filetype detection
filetype indent on    " Enable filetype-specific indenting
filetype plugin on    " Enable filetype-specific plugins
autocmd FileType ruby compiler ruby

"Code Folding
"set foldmethod=indent
"set nofoldenable
"set foldlevel=99

"Tab Options
set tabpagemax=15
map! <C-Right> <Esc>:tabn<CR>
map! <C-Left> <Esc>:tabp<CR>
nmap <C-Right> <Esc>:tabn<CR>
nmap <C-Left> <Esc>:tabp<CR>

"Redo
"nmap r <C-r>

"Mark the current line
set cursorline

"Show unwanted white spaces
set list
set listchars=tab:>-,trail:~,extends:>,precedes:<

"Search option
set ignorecase
set hlsearch
nnoremap <CR> :nohlsearch<CR><CR>

"Make backspace work
set backspace=indent,eol,start

"Highlight exceding character in the 80th colunm
"highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"match OverLength /\%81v.\+/
set colorcolumn=80

set rtp+=/usr/local/opt/fzf

"Font Style and Size - GUI
if has("gui_running")
  if has("gui_gtk2")
    set guifont=Inconsolata\ 12
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif

"Copy and paste from and to clipboard
if has("unix")
  let s:uname = system("uname -s")
    " regex to check if it is a Mac OS
    if s:uname =~ "darwin"
      command Copyfile w !pbcopy
      command Copyline .w !pbcopy

      nmap <F6> :set paste<CR>:r !pbpaste<CR>:set nopaste<CR>
      imap <F6> <Esc>:set paste<CR>:r !pbpaste<CR>:set nopaste<CR>
      nmap <F7> :.w !pbcopy<CR><CR>
      vmap <F7> :w !pbcopy<CR><CR>
    else
      command Copyfile w !xclip -selection clipboard
      command Copyline .w !xclip -selection clipboard

      nmap <F6> :set paste<CR>:r !xclip -selection clipboard -o <CR>:set nopaste<CR>
      imap <F6> <Esc>:set paste<CR>:r !xclip -selection clipboard -o <CR>:set nopaste<CR>
      nmap <F7> :.w !xclip -selection clipboard<CR><CR>
      vmap <F7> :w !xclip -selection clipboard<CR><CR>
    endif
endif

" Ignore the following extensions when you try to autocomplete file path
set wildignore=*.o,*.pyc,*~,*.swp,*.swo

" Incrementally searchs while typing
set incsearch

" Always shows the line and colunm on the bottom left of the screen
set ruler

" Always keeps N lines before and after the cursor line
set scrolloff=15

" Ignores files with those extensions in the file explorer
let g:netrw_list_hide= '^[.]*.*\.s[a-w][a-z]$,^.*\.py[co]$,^.*DS_Store$,^.*\.o$,^.*\~$'

" Tips and Tricks
" Access the serach buffer: ctrl + r and slash (/)
" Access the "yank" buffer: ctrl + r and quotes (")
" Access the word under the cursor: ctrl + r and ctrl + w

" MINGW cursor
" let &t_ti.="\e[1 q"
" let &t_SI.="\e[5 q"
" let &t_EI.="\e[1 q"
" let &t_te.="\e[0 q"

set belloff=all
